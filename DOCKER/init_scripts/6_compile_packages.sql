conn ptoventa/mundial265

Spool paquetes_bd_nvo.log

@paquetes_bd_nvo.sql

set serveroutput on

exec dbms_utility.compile_schema('PTOVENTA',false);

select OBJECT_NAME from ALL_OBJECTS where STATUS='INVALID';

declare
cant INT;
begin
select count(1) INTO cant from ALL_OBJECTS where STATUS='INVALID';
DBMS_OUTPUT.PUT_LINE('OBJECTS_INVALID:'||cant||'.');
end;
/
