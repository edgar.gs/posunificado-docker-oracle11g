
$ptoventa = "mundial265"

##
$TempFile = "actualizar_version.bat"
New-Item $TempFile -Force

echo @"
rem for /f "skip=4 tokens=1" %%a in ('net files') do net files %%a /close
sqlplus /nolog @cambios_bd.sql
rem for /f "skip=4 tokens=1" %%a in ('net files') do net files %%a /close

"@  | Out-File -FilePath $TempFile -Encoding ASCII -Force

##
$TempFile = "cambios_bd.sql"
New-Item $TempFile -Force

echo @"
-- Connect sys/namego@XE as sysdba
-- @scripts/kill_farmaventa_id.sql
Connect ptoventa/$ptoventa@XE
Spool paquetes_bd_nvo.log
Set define off
@Script1.sql
@paquetes_bd_nvo.sql
-- @Script2.sql
Connect sys/namego@XE as sysdba
BEGIN UTL_RECOMP.recomp_serial(); END;
/
Connect ptoventa/$ptoventa@XE
Exec dbms_utility.compile_schema('PTOVENTA',FALSE);
set serveroutput on

declare
 pase_version VARCHAR2(100) := 'POSU $version';
 ccod_local varchar2(100);
 ccod_cia CHAR(3);
 cinvalidos varchar2(5);
 
  --ERIOS 01.09.2014 Procedimiento compila paquetes
  PROCEDURE compila_paquetes 
  is
  CURSOR curPackage IS
  select DISTINCT OBJECT_NAME 
  from all_objects 
  where 1=1
  AND status <> 'VALID' 
  and owner='PTOVENTA'
  AND (OBJECT_TYPE = 'PACKAGE' OR OBJECT_TYPE = 'PACKAGE BODY')
  --AND OBJECT_NAME = 'FASA_INT_CONFIRMACION'
  ;  
	BEGIN
	  FOR rowPackage IN curPackage
	  LOOP
		BEGIN
			--DBMS_OUTPUT.PUT_LINE('ALTER PACKAGE '||rowPackage.OBJECT_NAME||' COMPILE;');		
			EXECUTE IMMEDIATE 'ALTER PACKAGE '||rowPackage.OBJECT_NAME||' COMPILE';
		EXCEPTION
			WHEN OTHERS THEN
				DBMS_OUTPUT.PUT_LINE('ERROR AL COMPILAR '||rowPackage.OBJECT_NAME||':'||sqlerrm);
		END;
	  END LOOP;	
	END;

begin
    --ERIOS 01.09.2014 Compila paquetes
	compila_paquetes;

end;
/
quit
"@  | Out-File -FilePath $TempFile -Encoding ASCII -Force

##
$logFile = "Script1.sql"
New-Item $logFile -Force

$libNames = (Get-ChildItem fps-app-bd-posunificado\SCRIPTS\*.sql).Name
foreach ($libName in $libNames)
{
    if($libName -ne "06_ControlVersion.sql") {
      $cp = "@scripts/$libName"
      Add-Content $logFile $cp
    }
}

##
$logFile = "paquetes_bd_nvo.sql"
New-Item $logFile -Force

$libNames = (Get-ChildItem fps-app-bd-posunificado\PTOVENTA\Packages\*.pck).Name
foreach ($libName in $libNames)
{
    $cp = "@paquetes/$libName"
    Add-Content $logFile $cp
}